
import Foundation

struct TestResult: Decodable {
    var dbn: String!
    var schoolName: String!
    var numberOfSatTestTakers: String!
    var satCriticalReadingAvgScore: String!
    var satMathAvgScore: String!
    var satWritingAvgScore: String!
    
    init() {
        
    }
    
    init(dbn: String, schoolName: String, numberOfSatTestTakers: String, satCriticalReadingAvgScore: String, satMathAvgScore: String, satWritingAvgScore: String) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.numberOfSatTestTakers = numberOfSatTestTakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
}
