
import Foundation
import Moya

enum APIService {
    case getSchools
    case getSchoolTestResults
}
// https://data.cityofnewyork.us/resource/s3k6-pzi2.json
// https://data.cityofnewyork.us/resource/f9bf-2cp4.json

// Would be using oauth or similar to access API
// Project specified to use these URLs
// API would provide paging, search and query

extension APIService: TargetType {
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    static let baseURLPath = "https://data.cityofnewyork.us"
    
    var baseURL: URL { return URL(string: APIService.baseURLPath)! }
    
    var path: String {
        switch self {
        case .getSchools:
            return "/resource/s3k6-pzi2.json"
        case .getSchoolTestResults:
            return "/resource/f9bf-2cp4.json"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getSchools:
            return .get
        case .getSchoolTestResults:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .getSchools:
            return .requestParameters(parameters: [String : Any](), encoding: URLEncoding.default)
        case .getSchoolTestResults:
            return .requestParameters(parameters: [String : Any](), encoding: URLEncoding.default)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getSchools:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        case .getSchoolTestResults:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        }
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
