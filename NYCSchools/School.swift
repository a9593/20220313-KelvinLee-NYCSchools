
import Foundation

struct Schools<T: Decodable>: Decodable {
    let results: [T]
}

struct School: Decodable {
    var dbn: String!
    var schoolName: String!
    var city: String!
    
    var testResults: TestResult?
    
    init() {
        
    }
    
    init(dbn: String, schoolName: String, city: String) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.city = city
    }
}
