
import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var numberOfTestTakersLabel: UILabel!
    @IBOutlet weak var numberOfTestTakersNumberLabel: UILabel!
    
    @IBOutlet weak var satCriticalReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var satCriticalReadingAvgScoreNumberLabel: UILabel!
    
    @IBOutlet weak var satMathAvgScoreLabel: UILabel!
    @IBOutlet weak var satMathAvgScoreNumberLabel: UILabel!
    
    @IBOutlet weak var satWritingAvgScoreLabel: UILabel!
    @IBOutlet weak var satWritingAvgScoreNumberLabel: UILabel!
    
    
    var testResult: TestResult?
    var dbn: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Test Results"
        
        NetworkService.shared.getSchoolTestResults(dbn: self.dbn).get { data in
            self.testResult = data
        }.ensure {
            if let testResult = self.testResult,
               let numberOfSatTestTakers = testResult.numberOfSatTestTakers,
               let satCriticalReadingAvgScore = testResult.satCriticalReadingAvgScore,
               let satMathAvgScore = testResult.satMathAvgScore,
               let satWritingAvgScore = testResult.satWritingAvgScore {
                self.numberOfTestTakersLabel.text = "Number of SAT Test Takers"
                self.numberOfTestTakersNumberLabel.text = "\(numberOfSatTestTakers)"
                
                self.satCriticalReadingAvgScoreLabel.text = "SAT Critical Reading Avg Score"
                self.satCriticalReadingAvgScoreNumberLabel.text = "\(satCriticalReadingAvgScore)"
                
                self.satMathAvgScoreLabel.text = "SAT Math Avg Score"
                self.satMathAvgScoreNumberLabel.text = "\(satMathAvgScore)"
                
                self.satWritingAvgScoreLabel.text = "SAT Writing Avg Score"
                self.satWritingAvgScoreNumberLabel.text = "\(satWritingAvgScore)"
                
            } else {
                self.numberOfTestTakersLabel.text = "Number of SAT Test Takers"
                self.numberOfTestTakersNumberLabel.text = "-"
                
                self.satCriticalReadingAvgScoreLabel.text = "SAT Critical Reading Avg Score"
                self.satCriticalReadingAvgScoreNumberLabel.text = "-"
                
                self.satMathAvgScoreLabel.text = "SAT Math Avg Score"
                self.satMathAvgScoreNumberLabel.text = "-"
                
                self.satWritingAvgScoreLabel.text = "SAT Writing Avg Score"
                self.satWritingAvgScoreNumberLabel.text = "-"
            }
            
        }.catch { error in
            print("Error")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
