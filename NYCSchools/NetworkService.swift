
import Foundation
import Moya
import PromiseKit

struct ErrorMessage: Error {
    var message: String!
    
    init(message: String) {
        self.message = message
    }
}

struct NetworkService {
    static let shared = NetworkService()
    private let provider = MoyaProvider<APIService>()
    
    func getSchools() -> Promise<[School]> { return Promise { seal in
            provider.request(.getSchools, completion: { (response) in
                switch response {
                case let .success(response):
                    do {
                        let filteredResponse = try response.filterSuccessfulStatusCodes()
                        let json = try filteredResponse.mapJSON() as! [[String: String]]
                        var schools = [School]()
                        // Would use decodable instead of manually parsing JSON
                        for data in json {
                            if let dbn = data["dbn"], let schoolName = data["school_name"], let city = data["city"] {
                                let school = School(dbn: dbn, schoolName: schoolName, city: city)
                                schools.append(school)
                            }
                        }
                        seal.fulfill(schools)
                    } catch {
                        print("Parse error")
                        seal.reject(ErrorMessage(message: response.debugDescription))
                    }
                case .failure(_):
                    print("Error")
                    seal.reject(ErrorMessage(message: "Request error"))
                }
            })
        }
    }
    
    func getSchoolTestResults(dbn: String) -> Promise<TestResult?> { return Promise
        { seal in
        provider.request(.getSchoolTestResults, completion: { (response) in
            switch response {
            case let .success(response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let json = try filteredResponse.mapJSON() as! [[String: String]]
                    var testResults = [TestResult]()
                    // Would use decodable instead of manually parsing JSON
                    for data in json {
                        if let dbn = data["dbn"], let schoolName = data["school_name"], let numberOfSatTestTakers = data["num_of_sat_test_takers"], let satCriticalReadingAvgScore = data["sat_critical_reading_avg_score"], let satMathAvgScore = data["sat_math_avg_score"], let satWritingAvgScore = data["sat_writing_avg_score"] {
                            let testResult = TestResult(dbn: dbn, schoolName: schoolName, numberOfSatTestTakers: numberOfSatTestTakers, satCriticalReadingAvgScore: satCriticalReadingAvgScore, satMathAvgScore: satMathAvgScore, satWritingAvgScore: satWritingAvgScore)
                            testResults.append(testResult)
                        }
                    }
                    if let testResult = testResults.filter({ $0.dbn == dbn }).first {
                        seal.fulfill(testResult)
                    } else {
                        seal.fulfill(nil)
                    }
                } catch {
                    print("Parse error")
                    seal.reject(ErrorMessage(message: response.debugDescription))
                }
            case .failure(_):
                print("Error")
                seal.reject(ErrorMessage(message: "Request error"))
            }
        })
        }
    }
}
