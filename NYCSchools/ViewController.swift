//
//  ViewController.swift
//  20220313-KelvinLee-NYCSchools
//
//  Created by Kelvin Lee on 3/13/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var schools = [School]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Schools"
        
        // Would use paging for optimization, so not to load everything all at once
        // Would add sorting and filtering to organize the list
        
        NetworkService.shared.getSchools().done { data in
            self.schools = data
            self.tableView.reloadData()
        }.catch { error in
            print("VC error:\(error)")
        }

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NetworkService.shared.getSchools().get { data in
            self.schools = data
        }.ensure {
            self.tableView.reloadData()
        }.catch { error in
            print("VC error:\(error)")
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schools.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell") as! SchoolTableViewCell
        let school = schools[indexPath.row]
        cell.schoolNameLabel.text = school.schoolName
        cell.cityLabel.text = school.city
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let school = self.schools[indexPath.row]
        detailViewController.dbn = school.dbn
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

