//
//  SchoolTests.swift
//  NYCSchoolsTests
//
//  Created by Kelvin Lee on 3/13/22.
//

import XCTest
@testable import NYCSchools

class SchoolTests: XCTestCase {
    
    var schools: [School]?
    
    override func setUp() {
        super.setUp()
        
        
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.schools = nil
    }
    
    func testSchoolsRequest() {
        NetworkService.shared.getSchools().done { (schools) in
            
            self.schools = schools
            
            XCTAssertFalse(schools.isEmpty)
            
        }.catch { (error) in
            XCTFail()
        }
        
    }
    
    func testSchoolHasId() {
        if let firstSchool = self.schools?.first {
            XCTAssertFalse(firstSchool.dbn.isEmpty)
        }
    }
    
    func testSchoolTestResultsRequest() {
        if let firstSchool = self.schools?.first {
            NetworkService.shared.getSchoolTestResults(dbn: firstSchool.dbn).done { (testResult) in
                if let testResult = testResult {
                    XCTAssertNotNil(testResult.numberOfSatTestTakers)
                } else {
                    XCTFail()
                }
            }.catch { (error) in
                XCTFail()
            }
        }
        
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
